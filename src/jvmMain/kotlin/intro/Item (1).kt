package intro
data class Item(val name: String, val done: Boolean)

interface ItemRepository{
    fun list() : List<Item>
    fun insert(item: Item)
}

//class RealmItem(val ): RealmObject

class InMemoryItemRepository : ItemRepository{
    private val items = mutableListOf<Item>()

    override fun list(): List<Item> = items.toList()

    override fun insert(item: Item) {
        items.add(item)
    }
}

object ServiceLocator {
    val itemRepository : ItemRepository = InMemoryItemRepository()
}

fun main() {
    val itemRepository = ServiceLocator.itemRepository
    itemRepository.insert(Item("Some task", true))
    println(itemRepository.list())
}

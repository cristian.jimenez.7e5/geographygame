package intro// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import androidx.compose.material.MaterialTheme
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application

sealed interface Screen{
    object NameScreen : Screen
    class WelcomeScreen(val userName: String)  : Screen
}

@Composable
@Preview
fun AppMateu() {
    var screen by remember { mutableStateOf<Screen>(Screen.NameScreen) }

    MaterialTheme {
        when(val currentScreen = screen){
            Screen.NameScreen -> NameScreen(onNameSet = { username ->
                screen = Screen.WelcomeScreen(username)
            })
            is Screen.WelcomeScreen -> WelcomeScreen(currentScreen.userName)
        }
    }
}

@Composable
fun WelcomeScreen(userName: String) {
    Text(userName)
}

@Composable
fun NameScreen(onNameSet : (String) -> Unit) {
    var name by remember { mutableStateOf("") }
    Column {
        TextField(
            value = name,
            label = { Text("Your Name") },
            onValueChange = { name = it }
        )
        Button(onClick = { onNameSet(name )}){
            Text("Submit")
        }
    }

}

fun main() = application {
    Window(onCloseRequest = ::exitApplication) {
        App()
    }
}

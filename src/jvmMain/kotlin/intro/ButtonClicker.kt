import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application

fun main() = application {
    Window(onCloseRequest = ::exitApplication) {
        ButtonClickerApp()
    }
}

@Composable
@Preview
fun ButtonClickerApp() {
    var counter by remember {
        mutableStateOf(0)
    }

    Column {
        Text("Nombre de clicks: $counter")
        Button(onClick = {
            counter++
        }){
            Text("Increase counter")
        }
    }
}

package GeographyGame

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Country(
    @SerialName("name") val countryName: CountryName,
    @SerialName("capital") val countryCapital: List<String> = listOf(),
    val flag: String
)

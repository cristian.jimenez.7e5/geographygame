package GeographyGame

sealed interface Screen{
    object NameScreen : Screen
    class WelcomeScreen(val userName: String)  : Screen
    class GameScreen: Screen
    class GameOver(val score: Int) : Screen
}
package GeographyGame

import kotlinx.serialization.Serializable

@Serializable
data class CountryName(val common: String)

package GeographyGame

data class Question(val chosenCapital: List<String>, val answerOptions: List<Country>)

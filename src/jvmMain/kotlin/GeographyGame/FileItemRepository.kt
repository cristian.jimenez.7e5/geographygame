package GeographyGame

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class FileItemRepository: CountryRepository{
    private val countriesList: List<Country>

    init {
        val json = this::class.java.getResource("/countriesData.json").readText(Charsets.UTF_8)
        countriesList = Json { ignoreUnknownKeys = true }.decodeFromString(json)
    }

    override fun list(): List<Country> = countriesList.toList()
}

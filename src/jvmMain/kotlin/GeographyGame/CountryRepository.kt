package GeographyGame

interface CountryRepository{
    fun list(): List<Country>
}

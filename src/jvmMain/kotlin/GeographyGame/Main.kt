package GeographyGame

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun main()  = application{
    Window(onCloseRequest = ::exitApplication) {
        App()
    }
}

@Composable
@Preview
fun App() {
    var screen by remember { mutableStateOf<Screen>(Screen.NameScreen) }
    val itemRepository = ServiceLocator.itemRepository
    val countriesList = itemRepository.list()
    var userName = ""

    MaterialTheme {
        when(val currentScreen = screen){
            Screen.NameScreen -> NameScreen(onNameSet = { username ->
                userName = username
                screen = Screen.WelcomeScreen(username)
            })
            is Screen.WelcomeScreen -> WelcomeScreen(currentScreen.userName, onClickPlayButton = {
                screen = Screen.GameScreen()
            })
            is Screen.GameScreen -> GeographyGameApp(countriesList, onEndGame = { score ->
                screen = Screen.GameOver(score)
            })
            is Screen.GameOver -> GameOver(currentScreen.score, userName)
        }
    }
}

@Composable
fun NameScreen(onNameSet: (String) -> Unit) {
    var name by remember { mutableStateOf("") }
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.padding(horizontal = 16.dp)
        ) {
            TextField(
                value = name,
                label = { Text("Your Name") },
                onValueChange = { name = it }
            )
            Spacer(modifier = Modifier.height(16.dp))
            Button(onClick = { onNameSet(name) }) {
                Text("Submit")
            }
        }
    }
}

@Composable
fun WelcomeScreen(userName: String, onClickPlayButton: () -> Unit) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.padding(top = 25.dp)
        ) {
            Text("Welcome $userName!")

            Spacer(modifier = Modifier.height(35.dp))

            Button(onClick = { onClickPlayButton() }) {
                Text("Play!")
            }
        }
    }
}


@Composable
fun GeographyGameApp(countriesList: List<Country>, onEndGame: (Int) -> Unit) {
    var currentQuestion by remember { mutableStateOf(getRandomQuestion(countriesList)) }
    var answerText by remember { mutableStateOf("") }
    var score by remember { mutableStateOf(0) }
    var questionsDone by remember { mutableStateOf(0) }

    fun onAnswerSelected(answer: Country) {
        questionsDone++
        if (answer.countryCapital[0] == currentQuestion.chosenCapital[0]) {
            score += 50
            answerText = "Correct!"
        } else {
            answerText = "Incorrect!"
        }

        if(questionsDone < 3){
            CoroutineScope(Dispatchers.Default).launch {
                delay(1000)
                answerText = ""
                currentQuestion = getRandomQuestion(countriesList)
            }
        } else{
            CoroutineScope(Dispatchers.Default).launch {
                delay(1000)
                onEndGame(score)
            }
        }

    }

    Column{
        Row(modifier = Modifier.background(Color.Cyan).fillMaxWidth(), verticalAlignment = Alignment.Top){
            //Text("Question nº ${questionsDone+1}: ${currentQuestion.chosenCapital[0]}",
            Text(currentQuestion.chosenCapital[0],
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(all = 16.dp),
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold
            )
        }

        LazyColumn(modifier = Modifier.padding(top = 25.dp, start = 30.dp)) {
            items(currentQuestion.answerOptions){ answerOption ->
                CountryOption(answerOption, onResponse = {
                    onAnswerSelected(answerOption)
                })
            }
        }

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            verticalAlignment = Alignment.Bottom){
            Text(text = "Score: $score",
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(all = 16.dp),
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold
            )
        }

        Spacer(modifier = Modifier.height(30.dp))

        Row(
            modifier = Modifier.background(Color.Cyan).fillMaxWidth(),
            verticalAlignment = Alignment.Bottom){
            Text(text = answerText,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(all = 16.dp),
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold
            )
        }
    }
}

@Composable
fun CountryOption(country: Country, onResponse: (String) -> Unit){
    Row(modifier = Modifier.padding(all = 16.dp)) {

        Text(country.flag, fontSize = 24.sp)

        // Add a horizontal space between the flag and the country name
        Spacer(modifier = Modifier.width(12.dp))

        Button(
            onClick = { onResponse(country.countryCapital[0]) },
            colors = ButtonDefaults.buttonColors(backgroundColor = Color.Cyan),
            shape = RoundedCornerShape(20.dp),
            elevation =  ButtonDefaults.elevation(
                defaultElevation = 10.dp,
                pressedElevation = 15.dp,
                disabledElevation = 0.dp
            )
        ){
            Text(text = country.countryName.common, fontSize = 20.sp)
        }
    }
}

fun getRandomQuestion(countriesList: List<Country>): Question {
    var chosenCapital = countriesList.random()
    while(chosenCapital.countryCapital.isEmpty()){
        chosenCapital = countriesList.random()
    }
    val answerOptions = mutableListOf<Country>()
    answerOptions.add(chosenCapital)
    while (answerOptions.size < 4) {
        val randomCountry = countriesList.random()
        if (!answerOptions.contains(randomCountry) && randomCountry.countryCapital.isNotEmpty()) {
            answerOptions.add(randomCountry)
        }
    }
    return Question(chosenCapital.countryCapital, answerOptions.shuffled())
}

@Composable
fun GameOver(score: Int, userName: String){
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.padding(top = 25.dp)
        ) {
            Text("Hey $userName!")

            Spacer(modifier = Modifier.height(35.dp))

            Text("Your final score was: $score")
        }
    }
}